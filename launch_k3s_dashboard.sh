#!/bin/bash

echo 'starting dashboard at http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/workloads?namespace=default'
kubectl proxy --address 0.0.0.0 --accept-hosts '.*'