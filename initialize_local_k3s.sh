#!/bin/bash

check_required_software() {
  if ! command kubectl > /dev/null 2>&1
  then
    echo "please install kubectl and helm" >&2
    echo "https://kubernetes.io/docs/tasks/tools/" >&2
    echo "https://helm.sh/docs/intro/install/" >&2
    exit 1
  fi

  if ! command helm > /dev/null 2>&1
  then
    echo "please install helm" >&2
    echo "https://helm.sh/docs/intro/install/" >&2
    exit 1
  fi
}

install_k3s() {
  if ! command k3s > /dev/null 2>&1
  then
    echo "installing k3s" >&2
    curl -sfL https://get.k3s.io | sh -s - --docker
  fi
}

start_k3s() {
  if [ "$(systemctl is-active k3s)" != "active" ]
  then
    echo "starting k3s" >&2
    sudo systemctl start k3s
    echo "waiting for k3s to start" >&2
    sleep 10
    while ! sudo k3s kubectl get nodes 2> /dev/null | grep '\bReady'
    do
      echo "waiting for node to be ready" >&2
      sleep 5
    done
    configure_user_kubectl
  fi
}

configure_user_kubectl() {
  while [ ! -f "/etc/rancher/k3s/k3s.yaml" ]
  do
    echo "waiting for kubectl config" >&2
    sleep 5
  done

  sudo cp /etc/rancher/k3s/k3s.yaml ~/.kube/k3s.config.yaml
  sudo chown $(id -u):$(id -g) ~/.kube/k3s.config.yaml
  chmod 700 ~/.kube/k3s.config.yaml
  echo "merge contents of ~/.kube/k3s.config.yaml with ~/.kube/config" >&2
}

check_docker_cgroupfs() {
  if [ ! -e /etc/docker/daemon.json ]
  then
    echo -e '{\n  "exec-opts": ["native.cgroupdriver=cgroupfs"]\n}' | sudo tee /etc/docker/daemon.json
  else
    if ! grep 'native.cgroupdriver=cgroupfs' /etc/docker/daemon.json > /dev/null 2>&1
    then
      sudo cp /etc/docker/daemon.json /etc/docker/daemon.json.bak
      echo -e '{\n  "exec-opts": ["native.cgroupdriver=cgroupfs"]\n}' | sudo tee /etc/docker/daemon.json
    fi
  fi
}

run_main() {
    echo "installing for ${USER}" >&2
    check_required_software
    check_docker_cgroupfs
    if ! echo "${PATH}" | grep '/usr/local/bin'
    then
      export PATH="/usr/local/bin:${PATH}"
    fi
    install_k3s
    start_k3s
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
  if [ "$EUID" -ne 0 ]
  then
    echo "some commands will run with sudo" >&2
    sleep 5
  fi
  run_main
fi
