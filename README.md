local [k3s](https://k3s.iot) development
###

[Installation](https://rancher.com/docs/k3s/latest/en/advanced/#using-docker-as-the-container-runtime)

The following installs k3s, using your existing docker system, and a systemd service. You must have docker
already installed and running on your machine. The instructions include a url to a shell
script to install docker, but you should change the version of docker installed to the [most recent version](https://docs.docker.com/engine/release-notes/).

The below installs the service, but does not automatically start it or enable it to restart on reboot Remove the INSTALL_* variables before installing if you want k3s to run all the time on your machine.

```bash
curl -sfL https://get.k3s.io | INSTALL_K3S_SKIP_ENABLE=true INSTALL_K3S_SKIP_START=true sh -s - --docker
sudo systemctl start k3s
```

### check docker cgroupdriver
https://github.com/madhuakula/kubernetes-goat/pull/54/files

### setup KUBECONFIG

k3s creates a config that you can copy to your ./kube/config file, and chown it to your user:group. Once you set your KUBECONFIG environment variable in ${HOME}/.bashrc to ${HOME}/.kube/config, all future shells will use this as the global
kube configuration. All logins to other servers will be added to this file over time (although if a temporary login token
is used it may expire). **Note** if you already have ~/.kube/config (you have used kubectl before), you may want to research
how to merge these together, or just remove it and start over.

```bash
sudo cp /etc/rancher/k3s/k3s.yaml ${HOME}/.kube/config
sudo chown $(id -u):$(id -g) ${HOME}/.kube/config
chmod 700 ${HOME}/.kube/config
```

#### Install and start k3s

The script `initialize_local_k3s.sh` does all of the above. It will prompt for your sudo password for some commands, but
does not, itself, need to run as sudo. It will use the environment variables INSTALL_K3S_SKIP_ENABLE and 
INSTALL_K3S_SKIP_START if present, otherwise, it will default to starting and enabling k3s to start after reboot.

```bash
./initialize_local_k3s.sh
```

#### Mount a HostPath

You can mount a local directory on your host machine into a container running in k3s, using its full path definition (e.g.
you cannot use ~/, ../, etc.). This is useful for live reloading of code that you are editing on a host application such as
VSCode, or for capturing output of code generator output such as rails g.
See the [Documentation](https://kubernetes.io/docs/concepts/storage/volumes/#hostpath-configuration-example).

An example has been provided in this repo. Note, this will run the pod with spec.containers[0].securityContext.runAsUser set to the output of the command `id -u` on your host. This way files created in the container use the same id to create files/directories in the pod as outside.

```bash
sed -i "s/runAsUser: 1000/runAsUser: $(id -u)/" test-hostpath.yaml
mkdir -p /tmp/test-hostpath
kubectl create -f test-hostpath.yaml
kubectl attach -ti test-hostpath
ls /mounted
echo "from the container" >> /mounted/test.from.inside
exit
ls -l /tmp/test-hostpath/test.from.inside
cat /tmp/test-hostpath/test.from.inside
echo "from the host" >> /tmp/test-hostpath/test.from.inside
kubectl attach -ti test-hostpath
cat /mounted/test.from.inside
rm /mounted/test.from.inside
exit
ls -l /tmp/test-hostpath
kubectl delete -f test-hostpath.yaml
rmdir /tmp/test-hostpath
git checkout -- test-hostpath.yaml
```

#### Host Local Development Application on a dedicated url for your project

K3s comes with an Ingress Controller called Traefik, and a LoadBalancer called Klipper preinstalled.
These allow you to create an [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/) object in your
application (similar to an Openshift Route) configured to route all requests to the k3s DNS in one of the following ways:

##### Application Specific Local Development URL
You can configure the Ingress with one or more spec.rules[].host entry. You can append .localhost
to the end of any string, and this will route to the localhost on your machine with that string
as the HOST in the request. Treafik will then forward this request on to the application.


```bash
kubectl create -f test-web-ingress.yaml
curl web-server.oasis.localhost
<html><body><h1>It works!</h1></body></html>
kubectl delete -f test-web-ingress.yaml
```

##### K3s wide Domain
You can configure the Ingress without any spec.rules[].host values, but include spec.rules[].http.paths[].path settings.
Then you can make requests to each path in k3s cluster domain url using k3s.localhost.

```
kubectl create -f whoami.yaml
curl http://k3s.localhost/foo
curl http://k3s.localhost/bar
kubectl delete -f whoami.yaml
```

### Uninstall K3s
The following will completely uninstall k3s.
Check [Documentation](https://rancher.com/docs/k3s/latest/en/installation/uninstall/) for latest command
```
/usr/local/bin/k3s-uninstall.sh
```

### Kubernetes Dashboard

A great dashboard to your k3s cluster, or any other cluster that you interact with,
is [Lens Desktop](https://k8slens.dev/desktop.html). It is designed to use your
${HOME}/.kube/config to configure a full GUI interface to your clusters and namespaces
(based on contexts in ${HOME}/.kube/config), very similar to the Openshift cluster web
GUI. You can view all resources in a namespace, view logs from running pods, and even
get a shell into a running pod for debugging.